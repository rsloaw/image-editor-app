const { ObjectID } = require('mongodb');
const mongoose = require('mongoose');

const ImageSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    userId: {
        type: ObjectID,
        required: false
    },
    url: {
        type: String,
        required: true
    },
    modifiedOn: {
        type: Date,
        required: false
    }
});

module.exports = mongoose.model('Image', ImageSchema);