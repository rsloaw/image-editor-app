const { ObjectID } = require('mongodb');
const mongoose = require('mongoose');

const ImageSchema = mongoose.Schema({
    email: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    password: {
        type: String,
        required: true,
        min: 6,
        max: 255
    }
});

module.exports = mongoose.model('User', ImageSchema);