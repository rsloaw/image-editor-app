const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Image = require('../models/image');
const verify = require('../auth/verifyToken');


//GET ALL USERS
router.get('/', verify, async (req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch (err) {
        res.json({ message: err });
    }
})

//ADD USER
// router.post('/', async (req, res) => {
//     const user = new User({
//         email: req.body.email,
//         password: req.body.password,
//     });

//     // CHECK IF USER ALREADY EXSISTS
//     User.findOne({
//         email: req.body.email
//     }).exec(async (err, u) => {
//         if (err)
//             res.json({ message: "Essa" });
//         if (u)
//             res.status(400).send({ message: "User is already exsits!" });
//         else {
//             //SAVE USER
//             try {
//                 const savedUser = await user.save();
//                 res.json({
//                     message: 'User created succesfully!',
//                     savedUser
//                 });
//             } catch (err) {
//                 res.json({ message: err });
//             }
//         }
//     })
// });

// //GET ALL USER IMAGES
// router.get('/:userId', async (req, res) => {
//     try {
//         const user = await User.findById(req.params.userId);
//         const images = await Image.find({ userId: req.params.userId });
//         res.json({
//             message: 'Images asign to user',
//             images
//         });
//     } catch (err) {
//         res.send({ message: err });
//     }
// })

// //DELETING USER
// router.delete('/:userId', async (req, res) => {
//     try {
//         const removedUser = await User.remove({ _id: req.params.userId });
//         res.json(removedUser);
//     } catch (err) {
//         res.json({ message: err });
//     }
// })

// //UPDATE USER
// router.patch('/:userId', async (req, res) => {
//     try {
//         const user = await User.updateOne(
//             { _id: req.params.userId },
//             {
//                 $set: {
//                     email: req.body.email,
//                     password: req.body.password
//                 },
//                 role: req.body.role
//             }
//         );
//         res.json(user);
//     } catch (err) {
//         res.json({ message: err });
//     }
// })

module.exports = router;