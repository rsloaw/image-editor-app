const express = require('express');
const router = express.Router();
const verify = require('../auth/verifyToken');
const jwt_decode = require('jwt-decode');
const Image = require('../models/image')

// SHOW IMAGES ASSIGN TO USER
router.get('/', verify, async (req, res) => {
    const token = req.header('auth-token');
    const decoded = jwt_decode(token);
    const images = await Image.find({ userId: decoded._id });
    res.json({
        message: 'Images asign to user',
        images
    });
})

// ADD IMAGES ASIGNED TO USER
router.post('/', verify, async (req, res) => {
    const token = req.header('auth-token');
    const decoded = jwt_decode(token);

    const image = new Image({
        name: req.body.name,
        description: req.body.description,
        userId: decoded._id,
        url: req.body.url
    });

    try {
        const savedImage = await image.save();
        res.json({ message: 'Image added successfully!', savedImage});
    } catch (err) {
        res.json({ message: err });
    }
})

//UPDATE IMAGE
router.patch('/:imageId', verify, async (req, res) => {
    try {
        const image = await Image.updateOne(
            { _id: req.params.imageId },
            {
                $set: {
                    name: req.body.name,
                    description: req.body.description,
                    url: req.body.url,
                    modifiedOn: Date.now().toString()
                }
            });
        res.json({ message: 'Modified succesfully!'});
    } catch (err) {
        res.json({ message: err });
    }
})

//DELETE IMAGE
router.delete('/:imageId', verify, async (req, res) => {
    try {
        const removedImage = await Image.remove({ _id: req.params.imageId });
        res.json({ message: 'Removed succesfully!'});
    } catch (err) {
        res.json({ message: err });
    }
})

module.exports = router;