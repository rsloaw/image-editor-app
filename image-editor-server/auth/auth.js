const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const CONFIG = require('../conifg');
const { registerValidation, loginValidation } = require('../validation');

// REGISTER USER
router.post('/register', async (req, res) => {

    //VALIDATE
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send({ message: 'Validation error', error: error.details[0].message });

    // CHECK IF USER ALREADY EXSISTS
    const userExsits = await User.findOne({ email: req.body.email });
    if (userExsits) return res.status(400).send('User already exists');

    //HASH PASSWORDS
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    //CREATE NEW USER
    const user = new User({
        email: req.body.email,
        password: hashedPassword,
    });

    //SAVE USER
    try {
        const savedUser = await user.save();
        res.json({
            message: 'User created succesfully!',
            userId: user._id
        });
    } catch (err) {
        res.json({ message: err });
    }
});

//LOGIN USER
router.post('/login', async (req, res) => {

    //VALIDATE
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send({ message: 'Validation error', error: error.details[0].message });

    // CHECK IF USER ALREADY EXSISTS
    const userExsits = await User.findOne({ email: req.body.email });
    if (!userExsits) return res.status(400).send('Email is not found!');

    //PASSWORD IS CORRECT
    const validPass = await bcrypt.compare(req.body.password, userExsits.password)
    if (!validPass) return res.status(400).send('Invalid password!')

    //CREATE AND ASIGN TOKEN
    const token = jwt.sign({ _id: userExsits._id}, CONFIG.TOKEN_SECRET);
    res.header('auth-token', token).send({ message: 'Login succesfully', token: token});

});


module.exports = router;