const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

const config = require('./conifg');

// IMPORT ROUTES
const userRoute = require('./routes/UserRoute');
const imageRoute = require('./routes/ImageRoute');
const authRoute = require('./auth/auth');

const USER = require('./models/user');

//MIDDLEWARE
app.use(bodyParser.json());
app.use(cors());


//ROUTERS
app.get('/', (req, res) => {
    res.send('Home!');
})

app.use('/users', userRoute);
app.use('/images', imageRoute);
app.use('/auth', authRoute);

//CONNECT TO DB
mongoose.connect(config.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.connection.on('error', function (err) {
    console.log('Error: Could not connect to MongoDB.');
});
mongoose.connection.on('connected', () => {
    console.log('Successfully conntected to MongoDB');
})
// mongoose.connection.close(() => {
//     console.log('Successfully closed conntection to MongoDB');
// });

//LISTEN
app.listen(config.PORT, function () {
    console.log(`HTTP Server runing at http://${config.HOST}:${config.PORT}`);
}) 