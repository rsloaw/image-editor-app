import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { ImagesService } from 'src/app/services/images.service';

@Component({
  selector: 'app-images-list',
  templateUrl: './images-list.component.html',
  styleUrls: ['./images-list.component.css']
})
export class ImagesListComponent implements OnInit {

  images = [];

  constructor(private imageService: ImagesService, private router: Router, private authService: AuthServiceService) { }

  token = this.authService.token;
  ngOnInit(): void {
    this.imageService.sendGetRequest().subscribe(result => {
      console.log(result.images);
      this.images = result.images;
    });
  }

  redirectToEditor(index) {
    this.imageService.imageObject = this.images[index];
    this.router.navigate(['editor']);
  }

  logout() {
    this.authService.token = null;
    this.router.navigate(['login'])
  }

  selectedFile: File

  onFileChanged(event) {
    this.selectedFile = event.target.files[0].name;
    console.log(this.selectedFile)
  }

  onUpload() {
    const json = {
      name: "name",
      description: "desc1",
      url: `../assets/${this.selectedFile}`
    }
    console.log(json);
    this.imageService.sendPostRequest(json).subscribe(result => {
      console.log(result)
    });
    setTimeout(()=> console.log("timeout 0.5s"), 500);
    this.ngOnInit();

  }

  deleteImage(index){
    this.imageService.sendDeleteRequest(this.images[index]._id).subscribe(result => {
      console.log(result);
    })
    setTimeout(()=> console.log("timeout 0.5s"), 500);
    this.ngOnInit();
  }
}
