import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ImagesService } from 'src/app/services/images.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {

  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;
  @ViewChild('canvasFHD', { static: true })
  canvasFHD: ElementRef<HTMLCanvasElement>;

  constructor(private imageService: ImagesService) { }



  private ctx: CanvasRenderingContext2D;
  private ctxFHD: CanvasRenderingContext2D;
  private picture = new Image();

  imageURL = null;
  ngOnInit(): void {
    console.log(this.imageURL);
    this.imageURL = this.imageService.imageObject.url;
    console.log(this.imageURL);
    //this.imageURL = "../../../assets/img.jpg";
    console.log(this.imageURL);
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.ctxFHD = this.canvasFHD.nativeElement.getContext('2d');
    this.ctx.canvas.width = 690;
    this.ctx.canvas.height = 690 / 2.34;

    this.fillCanvasWithImage(this.ctx);
  }

  fillCanvasWithImage(canvas){
    this.picture.src =  this.imageURL;
    this.picture.onload = () => {
    canvas.clearRect(0, 0, canvas.canvas.width, canvas.canvas.height);
    canvas.drawImage(this.picture, 0, 0 , canvas.canvas.width, canvas.canvas.height);
  }
  }

  makePhotoMonochrome(){
    const greyImage = new Image();
    console.log(this.picture.width);
    greyImage.src = this.greyScale(this.picture);
    this.imageURL = greyImage.src;
    this.fillCanvasWithImage(this.ctx);
  }

  makeFog(){
    const randomImage = new Image();
    console.log(this.picture.width);
    randomImage.src = this.fogEffect(this.picture);
    this.imageURL = randomImage.src;
    this.fillCanvasWithImage(this.ctx);
  }

  invertColours(){
    var imageData = this.ctx.getImageData(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

     this.invertEffect(imageData.data);

  // Update the canvas with the new data
      this.ctx.putImageData(imageData, 0, 0);
  }

  greyScale(imgObj) {
    const canvas = document.createElement('canvas');
    const canvasContext = canvas.getContext('2d');
    const imgW = imgObj.width;
    const imgH = imgObj.height;
    canvas.width = imgW;
    canvas.height = imgH;
    canvasContext.drawImage(imgObj, 0, 0);
    const imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    for (let y = 0; y < imgPixels.height; y++) {
        for (let x = 0; x < imgPixels.width; x++) {
              const i = (y * 4) * imgPixels.width + x * 4;
              const avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
              imgPixels.data[i] = avg;
              imgPixels.data[i + 1] = avg;
              imgPixels.data[i + 2] = avg;
        }
    }
    canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
    return canvas.toDataURL();
   }

   fogEffect(imgObj) {
    const canvas = document.createElement('canvas');
    const canvasContext = canvas.getContext('2d');
    const imgW = imgObj.width;
    const imgH = imgObj.height;
    canvas.width = imgW;
    canvas.height = imgH;
    canvasContext.drawImage(imgObj, 0, 0);
    const imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    for (let y = 0; y < imgPixels.height; y++) {
        for (let x = 0; x < imgPixels.width; x++) {
              const i = (y * 7) * imgPixels.width + x * 7;
              const avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
              imgPixels.data[i] = avg;
              imgPixels.data[i + 1] = avg;
              imgPixels.data[i + 2] = avg;
        }
    }
    canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
    return canvas.toDataURL();
   }

   invertEffect(data) {
    for (var i = 0; i < data.length; i+= 4) {
      data[i] = data[i] ^ 255; // Invert Red
      data[i+1] = data[i+1] ^ 255; // Invert Green
      data[i+2] = data[i+2] ^ 255; // Invert Blue
    }
}

  downloadCanvasAsImage(){
    let downloadLink = document.createElement('a');
    downloadLink.setAttribute('download', 'image.png');
    let canvas = document.getElementById('myCanvas');
    let dataURL = this.ctx.canvas.toDataURL('image/png');
    let url = dataURL.replace(/^data:image\/png/,'data:application/octet-stream');
    downloadLink.setAttribute('href', url);
    downloadLink.click();
  }
}
