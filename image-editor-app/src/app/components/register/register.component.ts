import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/services/register.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerFormGroup: FormGroup;
  constructor(private registerService: RegisterService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.registerFormGroup = new FormGroup({
      email: new FormControl(),
      password: new FormControl()
    })
  }

  registerProces() {
    if (this.registerFormGroup.valid) {
      this.registerService.register(this.registerFormGroup.value).subscribe(result => {
        if (result.message) {
          console.log(result.message);
        } else {
          alert(result.error)
        }
      })
    }
  }

}
