import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import axios from 'axios';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
import { AuthServiceService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {

  constructor(private http: HttpClient, private authService: AuthServiceService) { }

  public imageObject;
  public token = this.authService.token

  //private token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDA2YjAyNmQ1YWZmNjNjNmMwYzlhMmYiLCJpYXQiOjE2MTEwNzUwNjJ9.Q7igcoO-ta89l1OxH_XvAkjbv8FjSXbwKezaKRZBt38";

  sendGetRequest(): Observable<any> {
    console.log("Send request to server");
    return this.http.get(`${baseUrl}/images`, {
      headers: new HttpHeaders().set('auth-token', this.token)
    });
  }

  sendDeleteRequest(id) {
    console.log("Send delete image requests");
    return this.http.delete(`${baseUrl}/images/${id}`, {
      headers: new HttpHeaders().set('auth-token', this.token)
    })
  }


  sendPostRequest(data) {

    console.log("Send post request to server");
    console.log(data)
    return this.http.post(`${baseUrl}/images`, data, {
      headers: new HttpHeaders().set('auth-token', this.token)
    }

    );
  }
}