import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http:HttpClient) { }
  register(data):Observable<any>{
    console.log("Send request to server");
    return this.http.post(`${baseUrl}/auth/register`, data);
  }
}
