export class Image {
    _id: string;
    name: string;
    description: string;
    userId: string;
    modifiedOn: string;
}
